Hi Mom, you should just be able to open a terminal and 
copy / paste the following lines in order. The terminal icon
is the only icon on the left-hand bar, other than google chrome.
Enjoy :)

******************************************************************************
PASTE THIS COMMAND AND RE-OPEN THIS FILE EVERY DAY YOU CHECK ON THE PROJECT. *
                                                                             *
cd ~/instructions-for-mom && git pull                                        *
                                                                             *
******************************************************************************

********************************************
PASTE THESE LINES IN THE TERMINAL IN ORDER *
                                           *       ONLY RUN THIS LINE ONCE
cd ~/PlantHub                              *                 |
git checkout styling                       *                 |
npm i                                      *                 | 
npm run superinit     <--------------------*-----------------/   
npm run start                              *   
                                           *
********************************************

Now you should be able to open a google chrome tab, and put
the following line in the search bar, and then hit enter.

***********************************************************
PUT THIS LINE IN THE GOOGLE SEARCH BAR AND THEN HIT ENTER *
                                                          *
localhost:8000/                                           *
                                                          *
***********************************************************

